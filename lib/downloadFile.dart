import 'dart:io';
import 'package:ftpconnect/ftpConnect.dart';
import 'settings.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';


Future<String> getFilePath(String path) async {
  path = path.replaceFirst('music', 'musss');
  // Directory appDocumentsDirectory = await getTemporaryDirectory();
  Directory appDocumentsDirectory = await getApplicationDocumentsDirectory();
  String appDocumentsPath = appDocumentsDirectory.path;
  
  if (path.contains('/')){
    var lastIndex = path.lastIndexOf('/');
    var dirPath = path.substring(0, lastIndex);

    if(! Directory('$appDocumentsPath/$dirPath').existsSync()) {
      print('$appDocumentsPath/$dirPath');
      Directory('$appDocumentsPath/$dirPath').createSync(recursive: true);
      print(Directory('$appDocumentsPath/$dirPath').existsSync());
    }
  }
  return '$appDocumentsPath/$path';
}

downloadFromFtp(String path) async{
  FTPConnect ftpConnect = FTPConnect(ftpHost,user:ftpLogin, pass:ftpPass);
  await ftpConnect.connect();

  bool res = await ftpConnect.downloadFileWithRetry(path, File( await getFilePath(path)));
  await ftpConnect.disconnect();
  print(res);
  print(await getFilePath(path));
}