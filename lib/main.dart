import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kplayer/kplayer.dart';
import './utils.dart';
import './downloadFile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// StreamController for isDarkMode
final isDarkMode = ValueNotifier(true);

void main() {
  Player.boot();
  runApp(
    ValueListenableBuilder(
      valueListenable: isDarkMode,
      builder: (context, snapshot, w) {
        return MaterialApp(
          themeMode: isDarkMode.value ? ThemeMode.dark : ThemeMode.light,
          theme: ThemeData(
            brightness: isDarkMode.value ? Brightness.dark : Brightness.light,
          ),
          darkTheme: ThemeData.dark(),
          home: const MyApp(),
        );
      },
    ),
  );
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var player = Player.create(
      media: PlayerMedia.asset( 'assets/Introducing_flutter.mp3'),
      autoPlay: false
  );
  List<PathDuration> assetsList = [];

  int currentPlayer = 0;



  @override
  void initState()   {
    super.initState();
     playlist(context);
    loadPathSounds(context);
     downloadFromFtp('music/eminen/04 - Cleanin\' Out My Closet.mp3');
  }

  @override
  dispose() {
    super.dispose();
    player.dispose();
  }

  bool loading = false;

  @override
  Widget build(BuildContext context) {
    final bool runCupertinoApp = false;


    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('KPlayer'),
        // add action to open Github page
        actions: <Widget>[
          IconButton(icon: const Icon(Icons.open_in_new), onPressed: () {}),
        ],
      ),
      body: ListView.builder(
          itemCount: assetsList.length,
          itemBuilder: (context, index) {


            return ListTile(
                      leading: const Icon(Icons.audiotrack),
                      title: Text(assetsList[index].path.replaceAll('assets/', '')),
                      subtitle:Text( assetsList[index].duration),
                      trailing: IconButton(
                        icon: const Icon(Icons.play_arrow),
                        onPressed: () {
                        setState(() {
                          print('PlayerController.palyers.length ${PlayerController.palyers.length}');

                          if ( player != Null && !player.disposed) {
                            player.dispose();
                          }

                            player = Player.create(
                                media: PlayerMedia.asset(assetsList[index].path),
                                autoPlay: true,
                              )..init();
                            print(player.playing);
                          });
                        },
                      ),
                    );
            // return Card(
            //     elevation: 5.0,
            //     shape: RoundedRectangleBorder(
            //         borderRadius: BorderRadius.all(Radius.circular(10.0))),
            //     child: Padding(
            //       padding: const EdgeInsets.fromLTRB(10.0, 25, 10.0, 25),
            //       child: Center(
            //         child:
            //         Text(assetsList[index].replaceAll('assets/', '')),
            //       ),
            //     ));
          }),
      // body: ListView(
      //   children: [
      //     const SizedBox(height: 20),
      //     ListTile(
      //       leading: const Icon(Icons.language),
      //       title: const Text("Load from network"),
      //       subtitle: TextField(
      //         controller: _loadFromNetworkController,
      //         decoration:
      //             const InputDecoration(hintText: "URL of the media to load"),
      //       ),
      //       trailing: IconButton(
      //         icon: const Icon(Icons.play_arrow),
      //         onPressed: () {
      //           setState(() {
      //             print(player.status);
      //             if (!player.disposed) {
      //               player.dispose();
      //             }
      //             // Timer(Duration(seconds: 2), () {
      //               player = Future.delayed(Duration(seconds: 3), () => Player.create(
      //                 media: PlayerMedia.network(
      //                     "https://luan.xyz/files/audio/ambient_c_motion.mp3"),
      //                 autoPlay: true,
      //               )..init()) as PlayerController;
      //             // });
      //           });
      //         },
      //       ),
      //     ),
      //     const Divider(),
      //     // load from Assets
      //     ListTile(
      //       leading: const Icon(Icons.audiotrack),
      //       title: const Text("Load from assets"),
      //       subtitle: TextField(
      //         controller: _loadFromAssetController,
      //         decoration: const InputDecoration(hintText: "assets path"),
      //       ),
      //       trailing: IconButton(
      //         icon: const Icon(Icons.play_arrow),
      //         onPressed: () {
      //           setState(() {
      //             print(player.status);
      //             if (!player.disposed) {
      //               player.dispose();
      //             }
      //             print('before timer '+ DateTime.now().toString());
      //             Timer(Duration(seconds: 2), () =>
      //               // print('in timer'+ DateTime.now().toString());
      //               player = Player.create(
      //                 media: PlayerMedia.asset(_loadFromAssetController.text),
      //                 autoPlay: true,
      //               )..init()
      //            );
      //           });
      //         },
      //       ),
      //     ),
      //     const Divider(),
      //     // widget for log
      //     Padding(
      //       padding: const EdgeInsets.symmetric(horizontal: 24),
      //       child: DefaultTextStyle(
      //         style: const TextStyle(fontSize: 11, color: Colors.grey),
      //         child: PlayerBuilder(
      //             player: player,
      //             builder: (context, event, child) {
      //               return Column(
      //                 children: [
      //                   const Text(
      //                     "Logs",
      //                     style: TextStyle(fontSize: 20),
      //                   ),
      //                   const SizedBox(height: 10),
      //                   Table(
      //                     border: TableBorder.all(
      //                         width: 1, color: Colors.grey.withOpacity(0.5)),
      //                     children: [
      //                       TableRow(children: [
      //                         const Text("Duration",
      //                             style:
      //                                 TextStyle(fontWeight: FontWeight.bold)),
      //                         Text(player.duration.toString()),
      //                       ]),
      //                       TableRow(children: [
      //                         const Text("Position",
      //                             style:
      //                                 TextStyle(fontWeight: FontWeight.bold)),
      //                         Text(player.position.toString()),
      //                       ]),
      //                       TableRow(children: [
      //                         const Text("Volume",
      //                             style:
      //                                 TextStyle(fontWeight: FontWeight.bold)),
      //                         Text(player.volume.toString()),
      //                       ]),
      //                       TableRow(children: [
      //                         const Text("Speed",
      //                             style:
      //                                 TextStyle(fontWeight: FontWeight.bold)),
      //                         Text(player.speed.toString()),
      //                       ]),
      //                       TableRow(children: [
      //                         const Text("Loop",
      //                             style:
      //                                 TextStyle(fontWeight: FontWeight.bold)),
      //                         Text(player.loop.toString()),
      //                       ]),
      //                       TableRow(children: [
      //                         const Text("Status",
      //                             style:
      //                                 TextStyle(fontWeight: FontWeight.bold)),
      //                         Text(player.status.toString()),
      //                       ]),
      //                       TableRow(children: [
      //                         const Text("Media type",
      //                             style:
      //                                 TextStyle(fontWeight: FontWeight.bold)),
      //                         Text(player.media.type.toString()),
      //                       ]),
      //                       TableRow(children: [
      //                         const Text("Media resource",
      //                             style:
      //                                 TextStyle(fontWeight: FontWeight.bold)),
      //                         Text(player.media.resource.toString()),
      //                       ]),
      //                       TableRow(children: [
      //                         const Text("Platform Adaptive Player",
      //                             style:
      //                                 TextStyle(fontWeight: FontWeight.bold)),
      //                         // table display the platform adaptive player from the map Player.platforms
      //                         Table(
      //                           border: TableBorder.all(
      //                               width: 1,
      //                               color: Colors.grey.withOpacity(0.5)),
      //                           children: [
      //                             for (var platform in Player.platforms.entries)
      //                               TableRow(children: [
      //                                 Text("${platform.key}",
      //                                     style: const TextStyle(
      //                                         fontWeight: FontWeight.bold)),
      //                                 Text("${platform.value?.name}"),
      //                               ]),
      //                           ],
      //                         ),
      //                       ]),
      //                     ],
      //                   ),
      //                 ],
      //               );
      //             }),
      //       ),
      //     ),
      //   ],
      // ),
      bottomNavigationBar: BottomAppBar(
        child: SizedBox(
          height: 60,
          child: PlayerBar(player: player, options: [
            ValueListenableBuilder(
              valueListenable: isDarkMode,
              builder: (context, snapshot, w) {
                return SwitchListTile(
                  secondary: const Icon(Icons.brightness_2),
                  title: const Text("Dark mode"),
                  value: isDarkMode.value,
                  onChanged: (bool value) {
                    isDarkMode.value = !isDarkMode.value;
                  },
                );
              },
            ),
          ]),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          MethodChannel channel = const MethodChannel('kplayer_channel');
          var result = await channel.invokeMethod(
              'GetSystemVolume', 'Hello from flutter');
          print("result.toString()");
          print(result);
        },
        child: const Icon(Icons.replay),
      ),
    );
  }

  loadPathSounds(BuildContext context) async {
    List<PathDuration> list = [];
    var assetsFile = await DefaultAssetBundle.of(context).loadString('AssetManifest.json');
    final Map<String, dynamic> manifestMap = json.decode(assetsFile);

    for ( var k in manifestMap.keys.where((element) => element.contains('.mp3'))) {
      list.add(PathDuration(Uri.decodeFull(k)));
    }

    setState(() {
      assetsList = list;
    });
  } //loadPathSounds()
}
