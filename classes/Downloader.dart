import 'dart:io' as io;

import 'Service.dart';
import 'Track.dart';

class Downloader {

  var maxDownloads = 2;
  var maxBufferSize = 16 * 1024 * 1024 * 1024; // 16 GB
  var c = 0;
  var playlistPath = 'playlist.txt';
  var pid;
  var ramPath = '/';
  List<String> downloadingNow = [];
  List<String> dlLog = [];
  List<Track> dlist = [];

  Download() {
    __construct();
  }

  shouldDownload(Track tr) async => await tr.shouldBeDownloaded() &&
        tr.startTime
            .difference(DateTime.now())
            .inSeconds > 10 && !downloadingNow.contains(tr.name);

  checkPlaylistFile() async => await io.File(playlistPath).exists() &&
        await io.File(playlistPath).length() > 0;

  __construct() async {
    print("down. download started\n");


    try {
      if (!await checkPlaylistFile()) {
        return;
      }


      var buf = await io.File(playlistPath).readAsLinesSync();
      buf.where((item) => item.contains('h<')).map((item) async {
        var tr = Track(item);

        if (shouldDownload(tr)) {
          dlist.add(tr);
          dlLog.add('${DateTime.now()} ${tr.name}\n');
        }
        if (dlist.length == 2) return;
      });
      //led lamps
      // if(dlist.length == 0 &&
      // intval(file_get_contents(ramPath + "led.pipe")) <
      //     20) Service::green1();
//file_put_contents(ramPath + "dl.log", dl_log);
      var startTime = DateTime.now();
      for (var i = 0; i < dlist.length; i++) {
        if (DateTime
            .now()
            .difference(startTime)
            .inSeconds < 30) {
          addToFile(dlist[i].name);
          await getTrack(dlist[i].remotePath);
          removeFromFile(dlist[i].name);
        }
      }
    } catch (e) {
      print('down. ${DateTime.now()} dl.class.Error ${e}');
    }
  }


  addToFile(String path) {
    if (path.length > 2) {
      downloadingNow.add(path);
      return true;
    }
    return false;
  }

  removeFromFile(String path) {
    return downloadingNow.remove(path);
  }
}
