import 'dart:io' as io;
import 'Service.dart';
import 'constants.dart';

class Track {
  String name = '';
  String fileName = '';
  String remotePath = '';
  DateTime startTime = DateTime.now();

  var ms;
  var length;
  var lengthMs;
  int bitrate = 128;
  bool saved = false;
  int fadein = 0;
  int size = 0;
  var pid;
  bool isPlaying = false;
  int fifo = -1;

  Track(String line) {
//h<2018.08.15 17:10:25.769 00347874 00002000> tMM=000211 002.0F/000125 track=/music/Disco Re-Edit/Delegation - Where Is The Love (Secret Sun Dancefloor Edit)
    try {
      if ((line.length <= 20) || !line.contains('h<')) {
        return;
      }
      startTime = DateTime.parse(line.substring(2, 19));
      length = (int.parse(line.substring(26, 8)) / 1000).round();
      lengthMs = int.parse(line.substring(26, 8));
      ms = int.parse(line.substring(22, 3));
      fadein = int.parse(line.substring(35, 8));
      var j = line.length;
      var i = line.length;
      while ((j != 0) && ('=' != line.substring(j, 1))) {
        j--;
      }
      while ((i != 0) && ('/' != line.substring(i, 1))) {
        i--;
      }
      remotePath = '{line.substring(j + 1)}.mp3';
      name = '{line.substring(i + 1)}.mp3';
      name = cleanStr(name);
    } catch (e) {
      print(e);
    }
  }

  Track.fromParams(this.startTime, this.lengthMs, this.ms, this.fadein,
      this.remotePath, this.name) {
    try {
      if (fadein < 2000) fadein += 1000;

      fileName = 'bufPath{hash1(name)}';
      var dlName = 'ramPath{hash1(name)}_tmp';
      var size_128 = lengthMs /
          1000.0 *
          16384; //примерный размер в байтах - не совпал ни разу
      var size_256 = lengthMs /
          1000.0 *
          32768; //примерный размер в байтах - не совпал ни разу

    } catch (e) {
      print(e);
    }
  }

  play() {
    var currentPlayer = 0;
    var nextPlayer = 0;

    if (io.File(fileName).existsSync()) {
      print('tr player-$currentPlayer  no track $name in buffer\n\n');

      if (currentPlayer == 1) {
        nextPlayer = 0;
      } else {
        nextPlayer = 1;
      }
      io.File('${ramPath}currentPlayer')
          .writeAsStringSync(nextPlayer.toString());
      return false;
    }

    var fade = '';
    if (fadein > 0) {
      fade = '--fade-in=00:0${(fadein / 1000.0).roundToDouble()}';
    }

    //print( 'tr currentPlayer. '. 'fade in length is '.round(fadein/1000.0, 2).PHP_EOL;

    nextPlayer = 0;
    if (currentPlayer == 1) {
      nextPlayer = 0;
    } else {
      nextPlayer = 1;
    } //currentPlayer = !currentPlayer;

    num end_time = 0;

    while (true) {
      var diff = startTime.millisecondsSinceEpoch +
          ms / 1000.0 -
          DateTime.now().millisecondsSinceEpoch;
      if (diff <= 0.001 && diff >= -1) {
        //проверка милисекунд

        end_time = DateTime.now().millisecondsSinceEpoch + lengthMs / 1000.0;
        filePutContents(ramPath + 'playing_name', name);
        filePutContents(ramPath + 'end_play', end_time.toString());
        filePutContents(ramPath + 'currentPlayer', nextPlayer.toString());

        //print( 'tr currentPlayer. playing started. '.name.' diff_ms='.(DateTime.now().microsecondsSinceEpoch- (startTime*1.0+ms/1000.0)).PHP_EOL;
        // exec('aplay -q -t wav ${ramPath}pipe$currentPlayer > /dev/null &');
        //exec('madplay -Q fade -o wave:'.ramPath.'pipe'.currentPlayer.' '.file_name);
        // exec('madplay -Q fade -o wave:${ramPath}pipe$currentPlayer ${ramPath}decr$currentPlayer');
        break;
      }
      // usleep(10);
    }
    //print( 'tr currentPlayer. '.date('i:s').' playing ended\n';

    // str = 'h<'.date('Y.m.d H:i:s.', startTime).sprintf('%03d ',ms).sprintf('%08d ', length_ms).sprintf('%04d',fadein).'> track='.remote_path.PHP_EOL;
    // filePutContents(ramPath+'played.txt', str, FILE_APPEND);
    // check_played_file();

    //exec('rm '.tr_name);

    return end_time;
  }

  shouldBeDownloaded() async {
    return (!await io.File(fileName).exists() ||
        (await io.File(fileName).exists() &&
            await io.File(fileName).length() < 1000));
  }
}
